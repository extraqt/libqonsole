LibQonsole

Forked from KDE, LibQonsole is a pure Qt implementation of the feature rich terminal widget, obtained from removing the KDE parts of Konsole.
It supports all the features of Konsole, and will aim to maintain feature-parity with Konsole.


## Features
*
*
*
*


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/extraqt/libqonsole.git LibQonsole`
- Enter the `LibQonsole` folder
  * `cd LibQonsole`
- Configure the project - we use meson for project management
  * `meson setup .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`

### Dependencies:
* Qt6

### Known Bugs
* Please test and let us know.
