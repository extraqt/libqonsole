/*
 *  SPDX-FileCopyrightText: 2020-2020 Gustavo Carneiro <gcarneiroa@hotmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include "RegExpFilter.h"

namespace Qonsole {
    class ColorFilter : public RegExpFilter {
        public:
            ColorFilter();

            static const QRegularExpression ColorRegExp;

        protected:
            QSharedPointer<HotSpot> newHotSpot( int startLine, int startColumn, int endLine, int endColumn, const QStringList& capturedTexts ) override;
    };
}
