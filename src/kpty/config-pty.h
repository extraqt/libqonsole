/* Defined to the path of the PTY multiplexer device, if any */
#define PTM_DEVICE    "${PTM_DEVICE}"

#define HAVE_POSIX_OPENPT
#define HAVE_GETPT
#define HAVE_GRANTPT
#define HAVE_OPENPTY
#define HAVE_PTSNAME
#define HAVE_REVOKE
#define HAVE_UNLOCKPT
#define HAVE__GETPTY
#define HAVE_TCGETATTR
#define HAVE_TCSETATTR

// #define HAVE_LIBUTIL_H
// #define HAVE_UTIL_H
#define HAVE_PTY_H

#define HAVE_TERMIOS_H
#define HAVE_TERMIO_H
// #define HAVE_SYS_STROPTS_H
// #define HAVE_SYS_FILIO_H

// #define HAVE_UTEMPTER
#define HAVE_LOGIN
// #define HAVE_UTMPX
// #define HAVE_LOGINX
#define HAVE_STRUCT_UTMP_UT_TYPE
#define HAVE_STRUCT_UTMP_UT_PID
#define HAVE_STRUCT_UTMP_UT_SESSION
// #define HAVE_STRUCT_UTMP_UT_SYSLEN
#define HAVE_STRUCT_UTMP_UT_ID

#define HAVE_SYS_TIME_H
